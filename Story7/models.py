from django.db import models

# Create your models here.
class Status(models.Model):
    nama = models.CharField(max_length=50, blank=False)
    status = models.TextField(max_length=1000, blank=False)
    tanggal = models.DateTimeField(auto_now_add=True)
