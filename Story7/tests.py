from django.test import TestCase, Client
from django.urls import resolve
from django.shortcuts import reverse
from selenium import webdriver
from .models import *
from .views import *
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest


class Story7Test(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get(reverse('Story7:input'))
        self.assertEqual(response.status_code, 200)

    def test_story7_confirmationurl_is_exist(self):
        Status.objects.create(nama='Handi', status='coba-coba', tanggal='April 12, 2020, 6:34 a.m.')
        pemosting = Status.objects.get(nama='Handi', status='coba-coba')
        response = Client().get(reverse('Story7:konfirmasi', kwargs={'id':pemosting.id}))
        self.assertEqual(response.status_code, 200)

    def test_story7_using_index_function(self):
        found = resolve(reverse('Story7:input'))
        self.assertEqual(found.func, index)

    def test_story7_url_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story7_can_create_status(self):
        Status.objects.create(nama='Handi', status='coba-coba', tanggal='April 12, 2020, 6:34 a.m.')
        jumlah_status = Status.objects.all().count()
        self.assertEqual(jumlah_status,1)

    def test_story7_confirm_url_using_confirm_template(self):
        Status.objects.create(nama='Handi', status='coba-coba', tanggal='April 12, 2020, 6:34 a.m.')
        response = Client().get(reverse('Story7:konfirmasi', kwargs={'id':1}))
        self.assertTemplateUsed(response, 'confirm.html')
    
    def test_story7_confirm_using_confirmation_function(self):
        Status.objects.create(nama='Handi', status='coba-coba', tanggal='April 12, 2020, 6:34 a.m.')
        found = resolve(reverse('Story7:konfirmasi', kwargs={'id':1}))
        self.assertEqual(found.func, confirmation)

    def test_story7_url_can_save_status(self):
        response_post = Client().post(reverse('Story7:input'), {'nama':'Handi', 'status':'coba-coba', 'tanggal':'April 12, 2020, 6:34 a.m.'})
        self.assertEqual(response_post.status_code, 302)

    def test_story7_confirm_can_delete_status_if_no_button_press(self):
        Status.objects.create(nama='Handi', status='coba-coba', tanggal='April 12, 2020, 6:34 a.m.')
        response_post = Client().post(reverse('Story7:konfirmasi', kwargs={'id':1}), {'tindakan':'Tidak'})
        self.assertEqual(response_post.status_code, 302)

    # functional test
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    
    def tearDown(self):
        self.browser.quit()
    
    def test_inputForm_and_confirm(self):
        self.browser.get('http://127.0.0.1:8000/story7/')
        time.sleep(5)
        
        try:
            name_input = self.browser.find_element_by_id('id_nama')
            name_input.send_keys('Handi')
            status_input = self.browser.find_element_by_id('id_status')
            status_input.send_keys('Coba Coba')
            time.sleep(2)
            submit = self.browser.find_element_by_tag_name('input')
            status_input.submit()
            self.browser.get('http://127.0.0.1:8000/story7/confirm/1')
            konfirmasi = self.browser.find_element_by_class_name('btn btn-primary my-5 mx-3')
            konfirmasi.submit()
            time.sleep(2)
        except:
            pass