from django.shortcuts import render, redirect, reverse
from .models import Status
from .forms import StatusForm

def index(request):
    if request.method == 'POST':
        status = StatusForm(request.POST)
        if status.is_valid():
            post = status.save()
            id = post.id
        return redirect(reverse('Story7:konfirmasi', kwargs={'id':id}))
    status_form = StatusForm()
    posting = Status.objects.all()
    response = {'status_form': status_form, 'posting':posting}
    return render(request, 'index.html', response)

def confirmation(request, id):
    kode = id
    if request.method == 'POST':
        status1 = Status.objects.get(id=kode)
        if request.POST.get('tindakan') == 'Tidak':
            status1.delete()
        return redirect(reverse('Story7:input'))
    
    status_tmp = Status.objects.get(id=kode)
    response = {'status_tmp':status_tmp}
    return render(request, 'confirm.html', response)