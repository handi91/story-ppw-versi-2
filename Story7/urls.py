from django.urls import path
from .views import *

app_name = 'Story7'
urlpatterns = [
    path('', index, name='input'),
    path('confirm/<int:id>', confirmation, name='konfirmasi')
]