from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.shortcuts import reverse
from selenium import webdriver
from .views import *
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class Story10UnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 302)
    
    def test_bad_request_api(self):
        response = Client().get('/story10/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story10/api/v1/signup/')
        self.assertEqual(response.status_code, 400)

class Story10FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super().tearDown()

    def test_login_in_loginpage(self):
        User.objects.create_user('wanawana', 'wannawan@gmail.com', 'wana1234')

        self.browser.get(self.live_server_url + '/story10/auth/')
 
        time.sleep(5)

        username = self.browser.find_element_by_id("username")
        username.send_keys("wanawana")

        password = self.browser.find_element_by_id("password")
        password.send_keys("wana1234")

        login_button = self.browser.find_element_by_id("logInButton")
        login_button.click()

        time.sleep(5)
        self.assertIn('wanawana', self.browser.page_source) 

        logout_button = self.browser.find_element_by_class_name("btn-primary")
        logout_button.click()

        time.sleep(5)
        self.assertNotIn('wanawana', self.browser.page_source)

    def test_login_page_login_no_user(self):
        self.browser.get(self.live_server_url + '/story10/auth/')

        time.sleep(5)

        username = self.browser.find_element_by_id("username")
        username.send_keys("handiaja")

        password = self.browser.find_element_by_id("password")
        password.send_keys("wana123")

        login_button = self.browser.find_element_by_id("logInButton")
        login_button.click()

        time.sleep(5)

        self.assertIn('Username belum terdaftar, mohon periksa kembali.', self.browser.page_source)

    def test_login_page_wrong_password(self):
        User.objects.create_user('wanawana', 'wannawan@gmail.com', 'wana1234')

        self.browser.get(self.live_server_url + '/story10/auth/')

        time.sleep(5)

        username = self.browser.find_element_by_id("username")
        username.send_keys("wanawana")

        password = self.browser.find_element_by_id("password")
        password.send_keys("wanwan123")

        login_button = self.browser.find_element_by_id("logInButton")
        login_button.click()

        time.sleep(5)

        self.assertIn('Username atau Password yang anda masukkan salah, mohon periksa kembali.', self.browser.page_source)
    
    def test_create_account(self):
        self.browser.get(self.live_server_url + '/story10/auth/')

        time.sleep(5)

        sign_up_button = self.browser.find_element_by_id("createAccountLink")
        sign_up_button.click()

        time.sleep(5)

        username = self.browser.find_element_by_id("username1")
        username.send_keys("wanawana")

        email = self.browser.find_element_by_id("email1")
        email.send_keys("wannawan@gmail.com")

        password = self.browser.find_element_by_id("password1")
        password.send_keys("wana1234")

        
        sign_up_button = self.browser.find_element_by_id("signUpButton")
        sign_up_button.click()

        time.sleep(5)

        user = authenticate(username="wanawana", password="wana1234")
        self.assertTrue(user)

        sign_up_button = self.browser.find_element_by_id("createAccountLink")
        sign_up_button.click()

        time.sleep(5)

        # TEST DUPLICATE USERNAME
        username = self.browser.find_element_by_id("username1")
        username.clear()
        username.send_keys("wanawana")

        email = self.browser.find_element_by_id("email1")
        email.clear()
        email.send_keys("wannawan@gmail.com")

        password = self.browser.find_element_by_id("password1")
        password.clear()
        password.send_keys("1234abcd")

        sign_up_button = self.browser.find_element_by_id("signUpButton")
        sign_up_button.click()

        time.sleep(5)

        self.assertIn('Username sudah terpakai, mohon ganti dengan username lain', self.browser.page_source)
