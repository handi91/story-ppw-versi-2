from django.contrib import admin
from django.urls import path, include

from .views import *

app_name = 'Story10'
urlpatterns = [
    path('', index, name="homepage"),
    path('auth/', auth, name="authentication_page"),
    path('logout/', logout_view, name="logout"),
    path('api/v1/login/', api_login, name="login"),
    path('api/v1/signup/', api_sign_up, name="sign_up"),
]
