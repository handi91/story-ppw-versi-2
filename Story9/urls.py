from django.urls import path
from .views import index

app_name = 'Story9'
urlpatterns = [
    path('', index, name='book')
]