from django.db import models

class Buku(models.Model):
    id_buku = models.CharField(max_length=100, unique=True)
    judul = models.CharField(max_length=150)
    publisher = models.CharField(max_length=150,null=True)
    jumlah_like = models.IntegerField(default=0)
    authors = models.CharField(max_length=100, null=True)
    link_gambar = models.TextField()

    def __str__(self):
        return self.title