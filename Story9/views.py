from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def index(request):
    return render(request, 'book.html')

def get_data(request):
    q = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + q
    
    response_json = requests.get(url).json()
    return JsonResponse(response_json)

