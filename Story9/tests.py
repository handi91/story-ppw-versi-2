from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.shortcuts import reverse
from selenium import webdriver
from .models import *
from .views import *
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class Story9UnitTest(TestCase):
    def test_url_story9_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_using_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response,'book.html')
    
    # def test_add_books_to_database(self):
    #     book_id = 'ajHIkjyokW'
    #     like_count = 50
    #     title = "Rembulan Tenggelam di Wajahmu"
    #     publisher = "Gramedia"
    #     authors = "Tere Liye"
    #     Buku.objects.create(
    #         id_buku = book_id,
    #         judul = title,
    #         publisher = publisher,
    #         jumlah_like = like_count,
    #         authors = authors
    #     )

    #     data = Buku.objects.get(title=title)
    #     self.assertEqual(str(data), judul)


class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_title_and_search_box_exist_in_homepage(self):
        self.selenium.get(self.live_server_url+'/story9/')
        time.sleep(2)

    
