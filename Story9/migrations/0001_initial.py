# Generated by Django 3.0.3 on 2020-04-29 06:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Buku',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_buku', models.CharField(max_length=100, unique=True)),
                ('judul', models.CharField(max_length=150)),
                ('publisher', models.CharField(max_length=150, null=True)),
                ('jumlah_like', models.IntegerField(default=0)),
                ('authors', models.CharField(max_length=100, null=True)),
                ('link_gambar', models.TextField()),
            ],
        ),
    ]
