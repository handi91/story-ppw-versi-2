$(document).ready(function() {
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + 'untitled', 
        success: function (response) {
            $('#result').empty();

            var trHTML = '<div class="table-responsive-sm"><table class="table table-striped table-bordered">';
            trHTML += '<thead><tr><th scope="col">No</th><th scope="col">Cover</th><th scope="col">Book Title</th><th scope="col">Author</th><th scope="col">Publisher</th></tr></thead><tbody>';
            for (let i = 0; i < response.items.length; i++) {
                var imageLink = 'https://www.eric-forum.eu/wp-content/uploads/2019/04/no-thumbnail.jpg';
                if (!(response.items[i].volumeInfo.imageLinks === undefined)) {
                    imageLink = response.items[i].volumeInfo.imageLinks.smallThumbnail;
                }
                trHTML += '<tr><th scope="row">' + (i + 1) + '</th><td>' +'<img src="' + imageLink + '" alt="gambar" />' + '</td><td>' + response.items[i].volumeInfo.title + '</td><td>' + response.items[i].volumeInfo.authors + '</td><td>' + response.items[i].volumeInfo.publisher;
            }
            trHTML += '</tr></tbody></table></div>'
            $('#result').append(trHTML);
        }
    })
    $('button').click(function () {
        var key = $('#search').val();

        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key, 
            success: function (response) {
                $('#result').empty();

                var trHTML = '<div class="table-responsive-sm"><table class="table table-striped table-bordered">';
                trHTML += '<thead><tr><th scope="col">No</th><th scope="col">Cover</th><th scope="col">Book Title</th><th scope="col">Author</th><th scope="col">Publisher</th></tr></thead><tbody>';
                for (let i = 0; i < response.items.length; i++) {
                    var imageLink = 'https://www.eric-forum.eu/wp-content/uploads/2019/04/no-thumbnail.jpg';
                    if (!(response.items[i].volumeInfo.imageLinks === undefined)) {
                        imageLink = response.items[i].volumeInfo.imageLinks.smallThumbnail;
                    }
                    trHTML += '<tr><th scope="row">' + (i + 1) + '</th><td>' +'<img src="' + imageLink + '" alt="gambar" />' + '</td><td>' + response.items[i].volumeInfo.title + '</td><td>' + response.items[i].volumeInfo.authors + '</td><td>' + response.items[i].volumeInfo.publisher;
                }
                trHTML += '</tr></tbody></table></div>'
                $('#result').append(trHTML);
            }
        })
    })
})
