function ubahTema() {
    var body = document.getElementById("page-id");
    if (body.className !== 'toggled') {
        body.className = 'toggled';
        body.setAttribute('accord', 'indigo');
    } else {
        body.className = '';
        body.setAttribute('accord', 'green');
    }

    var btn = document.getElementById("btn");
    if (btn.className !== 'toggled') {
        btn.className = 'toggled';
    } else {
        btn.className = '';
    }
    
}
