$(document).ready(function () {
    $('.accordion a').click(function (j) {
        var dropDown = $(this).closest('li').find('p');

        $(this).closest('.accordion').find('p').not(dropDown).slideUp();


        dropDown.stop(false, true).slideToggle();

        j.preventDefault();
    });

    $('button.btn-secondary').click(function() {
        var naik = $(this).closest('li');
        naik.stop(false,true).insertBefore(naik.prev());
        
    });
    $('button.btn-dark').click(function() {
        var ini = $(this);
        var turun = $(this).closest('li');
        turun.stop(false, true).insertAfter(turun.next());
        
    });

})