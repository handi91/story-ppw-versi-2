from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.shortcuts import reverse
from selenium import webdriver
from .models import *
from .views import *
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get("/story8/")
        self.assertEqual(response.status_code, 200)
    
    def test_story8_use_index_func(self):
        response = resolve("/story8/")
        self.assertEqual(response.func, index)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_story8_functional(self):
        self.selenium.get(self.live_server_url+'/story8/')
        time.sleep(2)

        self.assertIn("Welcome!", self.selenium.page_source)
        self.assertIn('Inilah Saya:', self.selenium.page_source)
        time.sleep(2)

        self.selenium.find_element_by_name('aktivitas').click()
        time.sleep(2)
        self.assertIn("Belajar Online Karena Lagi PJJ", self.selenium.page_source)
        self.assertIn("Belajar PPW", self.selenium.page_source)
        self.assertIn("Belajar Bersabar", self.selenium.page_source)
        self.assertIn("Main Game Mobile Legend", self.selenium.page_source)
        time.sleep(2)

        self.selenium.find_element_by_name('organisasi').click()
        time.sleep(2)
        self.assertIn("Cuma Ada Satu", self.selenium.page_source)
        self.assertIn("KMBUI XXVIII 2020 Kestari", self.selenium.page_source)
        self.assertIn("Udah", self.selenium.page_source)
        time.sleep(2)

        self.selenium.find_element_by_name('panitia').click()
        time.sleep(2)
        self.assertIn("Cuma Ada Satu :(", self.selenium.page_source)
        self.assertIn("Perlap Makrab KMBUI 2019", self.selenium.page_source)
        self.assertIn("Sudah Itu Aja", self.selenium.page_source)
        time.sleep(2)

        self.selenium.find_element_by_name('prestasi').click()
        time.sleep(2)
        self.assertIn("Gak Punya Prestasi", self.selenium.page_source)
        self.assertIn("Mungkin Cuma Harapan", self.selenium.page_source)
        self.assertIn("I'll Do My Best", self.selenium.page_source)
        time.sleep(2)

        self.selenium.find_element_by_id('naik0').click()
        time.sleep(2)
        self.selenium.find_element_by_id('turun0').click()
        time.sleep(2)
        self.selenium.find_element_by_id('naik1').click()
        time.sleep(2)
        self.selenium.find_element_by_id('turun1').click()
        time.sleep(2)
        self.selenium.find_element_by_id('naik2').click()
        time.sleep(2)
        self.selenium.find_element_by_id('turun2').click()
        time.sleep(2)
        self.selenium.find_element_by_id('naik3').click()
        time.sleep(2)
        self.selenium.find_element_by_id('turun3').click()
        time.sleep(2)


    def test_change_theme(self):
        self.selenium.get(self.live_server_url+'/story8/')
        before = self.selenium.find_element_by_id('page-id')
        time.sleep(2)
        before_color = before.value_of_css_property('background')
        before_accordion = self.selenium.find_element_by_name('aktivitas')
        before_accordion_color = before_accordion.value_of_css_property('background')
        change_button = self.selenium.find_element_by_id('btn')
        change_button.click()

        after_color = self.selenium.find_element_by_id('page-id').value_of_css_property('background')
        after_accordion_color = self.selenium.find_element_by_name('aktivitas')

        self.assertNotEqual(before_color, after_color)
        self.assertNotEqual(before_accordion_color, after_accordion_color)

