from django.urls import path
from .views import index

app_name = 'Story8'
urlpatterns = [
    path('', index, name='index')
]